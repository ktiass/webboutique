/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;



/**
 *
 * @author TIASS
 */
public class Personne {
    
    protected Long id;
    protected String nom;
    protected String prenoms;
    protected LocalDate dateNaissance;

    public Personne() {
    }
    
    public int getAge() {
        return this.getAge(LocalDate.now());
    }
    
    public int getAge(LocalDate dateReference) {
        if (this.dateNaissance == null || dateReference == null) {
            throw new IllegalArgumentException("Le champs dateNaissance ou le paramètre dateReference est null.");
        }
        Period period = Period.between(this.dateNaissance, dateReference);
        return period.getYears() < 0 ? - period.getYears() : period.getYears();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personne other = (Personne) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Personne{" + "id=" + id + ", nom=" + nom + ", prenoms=" + prenoms + ", dateNaissance=" + dateNaissance + '}';
    }

}
