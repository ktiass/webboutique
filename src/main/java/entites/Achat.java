/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


/**
 *
 * @author TIASS
 */
public class Achat {
        private Long id;
    private LocalDateTime dateAchat;
    private double remise;
    private List<ProduitAchete> produitAchetes = new LinkedList<>();

    public Achat() {
    }
    
    public double getPrixTotal() {
        double total = - this.remise;
        for (ProduitAchete produitAchete : this.produitAchetes) {
            total += produitAchete.getPrixTotal();
        }
        // on renvoie 0 au cas la remise est supérieure
        return total < 0 ? 0. : total;
    }
    
    public double getRemiseTotal() {
        double total = this.remise;
        for (ProduitAchete produitAchete : this.produitAchetes) {
            total += produitAchete.getRemise();
        }
        return total;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDateTime dateAchat) {
        this.dateAchat = dateAchat;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public List<ProduitAchete> getProduitAchetes() {
        return produitAchetes;
    }

    public void setProduitAchetes(List<ProduitAchete> produitAchetes) {
        this.produitAchetes = produitAchetes;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", dateAchat=" + dateAchat + ", remise=" + remise + '}';
    }

}
