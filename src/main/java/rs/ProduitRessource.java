/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Produit;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import service.ProduitService;

/**
 *
 * @author TIASS
 */
@Path("/produit")
public class ProduitRessource {
    @POST
    public void ajouter(@QueryParam("idProduit") Long id, @QueryParam("libelle") String libelle,@QueryParam("prixUnitaire") double prixUnitaire) {
        // ajoute l'objet p dans la collection liste
        if(id == null || libelle ==null || prixUnitaire == 0.0){
            throw new IllegalArgumentException("les champs id,libelle,description,prixUnitaire,datePeremption sont vides");
        }
        Produit p = new Produit();
        p.setId(id);
        p.setLibelle(libelle);
        p.setPrixUnitaire(prixUnitaire);
       // p.setDatePeremption(datePeremption);
        
        ProduitService.ajouter(p);
        System.out.println("Add successfull");
    }

    @PUT
    public void modifier(@QueryParam("idProduit") Long id, @QueryParam("libelle") String libelle,@QueryParam("prixUnitaire") double prixUnitaire) {
        // remplace par e, l'objet Produit de la liste qui a même id que e
        if(id == null || libelle ==null || prixUnitaire == 0.0){
            throw new IllegalArgumentException("les champs id,libelle,description,prixUnitaire,datePeremption sont vides");
        }
        Produit p = new Produit();
        p.setId(id);
        p.setLibelle(libelle);
        p.setPrixUnitaire(prixUnitaire);
        //p.setDatePeremption(datePeremption);
        
        ProduitService.modifier(p);
    }

    @GET
    @Path("/{id}")
    public Produit trouver(@PathParam("id") Long id) {
        // renvoie l'objet Produit de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        return ProduitService.trouver(id);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id) {
        // retirer de la liste, l'objet Produit qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        ProduitService.supprimer(id);
    }

    @DELETE
    @Path("/delete")
    public void supprimer(@QueryParam("idProduit") Long id, @QueryParam("libelle") String libelle,@QueryParam("prixUnitaire") double prixUnitaire) {
        // retirer de la liste, l'objet Categorie passé en paramètre
        if(id == null || libelle ==null || prixUnitaire == 0.0){
            throw new IllegalArgumentException("les champs id,libelle,description,prixUnitaire,datePeremption sont vides");
        }
        Produit p = new Produit();
        p.setId(id);
        p.setLibelle(libelle);
        p.setPrixUnitaire(prixUnitaire);
       // p.setDatePeremption(datePeremption);
        
        ProduitService.supprimer(p);
    }

    @GET
    public List<Produit> lister() {
        return ProduitService.lister();
    }

    @GET
    @Path("/getbyparams")
    public List<Produit> lister(@QueryParam("debut") int debut,@QueryParam("nombre") int nombre) {
        return ProduitService.lister(debut, nombre);
    }
}
