/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Categorie;
import entites.Employe;
import java.time.LocalDate;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.CategorieService;
import service.EmployeService;

/**
 *
 * @author TIASS
 */
@Path("/employe")
public class EmployeRessource {
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void ajouter(Employe e) {
        // ajoute l'objet e dans la collection liste
        EmployeService.ajouter(e);
        System.out.println("Add successfull");
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Employe e) {
        // remplace par e, l'objet Employee de la liste qui a même id que e
        if(e == null){
            throw new IllegalArgumentException("les champs de categorie sont vides");
        }
        
        EmployeService.modifier(e);
        System.out.println("Add successfull");
    }

    @GET
    @Path("/{id}")
    public Employe trouver(@PathParam("id") Long id) {
        // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        return EmployeService.trouver(id);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Long id) {
        // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        EmployeService.supprimer(id);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/delete")
    public void supprimer(Employe e) {
        // retirer de la liste, l'objet Categorie passé en paramètre
        if(e == null){
            throw new IllegalArgumentException("les champs id,libelle,description sont vides");
        }
 
        
        EmployeService.supprimer(e.getId());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employe> lister() {
        return EmployeService.lister();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getbyparams")
    public List<Employe> lister(@QueryParam("debut") int debut,@QueryParam("nombre") int nombre) {
        return EmployeService.lister(debut, nombre);
    }
    
}
