/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Categorie;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import service.CategorieService;

/**
 *
 * @author TIASS
 */
@Path("/categorie")
public class CategorieRessource {
    

    @POST
    public void ajouter(@QueryParam("idCategorie") Integer id, @QueryParam("libelle") String libelle,@QueryParam("description") String description) {
        // ajoute l'objet e dans la collection liste
        if(id == null || libelle ==null || description == null){
            throw new IllegalArgumentException("les champs id,libelle,description sont vides");
        }
        Categorie e = new Categorie();
        e.setId(id);
        e.setLibelle(libelle);
        e.setDescription(description);
        
        CategorieService.ajouter(e);
        System.out.println("Add successfull");
    }

    @PUT
    public void modifier(@QueryParam("idCategorie") Integer id, @QueryParam("libelle") String libelle,@QueryParam("description") String description) {
        // remplace par e, l'objet Categorie de la liste qui a même id que e
        if(id == null || libelle ==null || description == null){
            throw new IllegalArgumentException("les champs id,libelle,description sont vides");
        }
        Categorie e = new Categorie();
        e.setId(id);
        e.setLibelle(libelle);
        e.setDescription(description);
        
        CategorieService.modifier(e);
    }

    @GET
    @Path("/{id}")
    public Categorie trouver(@PathParam("id") Integer id) {
        // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        return CategorieService.trouver(id);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") Integer id) {
        // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
        if(id == null){
            throw new IllegalArgumentException("le champs id est vide");
        }
        CategorieService.supprimer(id);
    }

    @DELETE
    @Path("/delete")
    public void supprimer(@QueryParam("idCategorie") Integer id, @QueryParam("libelle") String libelle,@QueryParam("description") String description) {
        // retirer de la liste, l'objet Categorie passé en paramètre
        if(id == null || libelle ==null || description == null){
            throw new IllegalArgumentException("les champs id,libelle,description sont vides");
        }
        Categorie e = new Categorie();
        e.setId(id);
        e.setLibelle(libelle);
        e.setDescription(description);
        
        CategorieService.supprimer(e);
    }

    @GET
    public List<Categorie> lister() {
        return CategorieService.lister();
    }

    @GET
    @Path("/getbyparams")
    public List<Categorie> lister(@QueryParam("debut") int debut,@QueryParam("nombre") int nombre) {
        return CategorieService.lister(debut, nombre);
    }
}
